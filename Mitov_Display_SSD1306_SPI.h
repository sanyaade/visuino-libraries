////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2018 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifndef _MITOV_DISPLAY_SSD1306_SPI_h
#define _MITOV_DISPLAY_SSD1306_SPI_h

#include <Mitov_Display_SSD1306.h>

#ifdef __TEST_FOR_DEBUG_PRINTS__
#define Serial UNGUARDED DEBUG PRINT!!!
#endif

namespace Mitov
{
//---------------------------------------------------------------------------
	template<uint8_t C_COMPinsConfig, typename T_SPI, T_SPI &C_SPI, typename T_CHIP_SELECT_IMPLEMENTATION, typename T_REGISTER_SELECT_IMPLEMENTATION, uint8_t WIDTH, uint8_t HEIGHT> class Display_odtSSD1306_SPI : public T_CHIP_SELECT_IMPLEMENTATION, public T_REGISTER_SELECT_IMPLEMENTATION
	{
	public:
		inline void SendPinsConfigCommand()
		{
			SendCommand( C_COMPinsConfig );
		}

		void SendCommand( uint8_t ACommand )
		{
			T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( true );
			T_REGISTER_SELECT_IMPLEMENTATION::SetPinValue( false );
			T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( false );

			C_SPI.transfer( ACommand );

			T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( true );
		}

		void SendBuffer( uint8_t *ABuffer )
		{
			T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( true );
			T_REGISTER_SELECT_IMPLEMENTATION::SetPinValue( true );
			T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( false );

			for (uint16_t i = 0; i < ( WIDTH * HEIGHT / 8 ); ++i )
				C_SPI.transfer( ABuffer[ i ] );

			T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( true );
		}

		inline void IntSystemInitStart() {} // Placeholder
		inline void IntSystemInitEnd() {} // Placeholder
	};
//---------------------------------------------------------------------------
	template<uint8_t C_COMPinsConfig, typename T_SPI, T_SPI &C_SPI, typename T_CHIP_SELECT_IMPLEMENTATION, typename T_REGISTER_SELECT_IMPLEMENTATION, uint8_t WIDTH, uint8_t HEIGHT> class Display_odtSH1106_SPI : public Display_odtSSD1306_SPI<C_COMPinsConfig, T_SPI, C_SPI, T_CHIP_SELECT_IMPLEMENTATION, T_REGISTER_SELECT_IMPLEMENTATION, WIDTH, HEIGHT> 
	{
		typedef Display_odtSSD1306_SPI<C_COMPinsConfig, T_SPI, C_SPI, T_CHIP_SELECT_IMPLEMENTATION, T_REGISTER_SELECT_IMPLEMENTATION, WIDTH, HEIGHT>  inherited;

	public:
		void SendBuffer( uint8_t *ABuffer )
		{
			inherited::SendCommand(SSD1306Const::SSD1306_SETLOWCOLUMN | 0x0);  // low col = 0
			inherited::SendCommand(SSD1306Const::SSD1306_SETHIGHCOLUMN | 0x0);  // hi col = 0
			inherited::SendCommand(SSD1306Const::SSD1306_SETSTARTLINE | 0x0); // line #0

			byte height=64;
			byte width=132; 
			height >>= 3;
			width >>= 3;
			// I2C
			byte m_row = 0;
			byte m_col = 2;
			int p = 0;
			for ( byte i = 0; i < height; i++)
			{
				// send a bunch of data in one xmission
				inherited::SendCommand(0xB0 + i + m_row);//set page address
				inherited::SendCommand(m_col & 0xf);//set lower column address
				inherited::SendCommand(0x10 | (m_col >> 4));//set higher column address

				for( byte j = 0; j < 8; j++)
				{
//					inherited::ChipSelectOutputPin.SendValue( true );
					T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( true );
//					inherited::DataCommandOutputPin.SendValue( true );
					T_REGISTER_SELECT_IMPLEMENTATION::SetPinValue( true );
//					inherited::ChipSelectOutputPin.SendValue( false );
					T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( false );

					for ( byte k = 0; k < width; k++, p++)
						C_SPI.transfer( ABuffer[ p ] );

//					inherited::ChipSelectOutputPin.SendValue( true );
					T_CHIP_SELECT_IMPLEMENTATION::SetPinValue( true );
				}
			}
		}
	};
//---------------------------------------------------------------------------
}

#ifdef __TEST_FOR_DEBUG_PRINTS__
#undef Serial
#endif

#endif
